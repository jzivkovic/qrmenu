package hr.mars.qrm.api.controller;

import hr.mars.qrm.api.dto.RestaurantDTO;
import hr.mars.qrm.domain.Dish;
import hr.mars.qrm.domain.Menu;
import hr.mars.qrm.domain.Restaurant;
import hr.mars.qrm.repository.DishCategoryRepository;
import hr.mars.qrm.service.MenuService;
import hr.mars.qrm.service.RestaurantService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;
import java.util.UUID;

@Controller
public class DailyMenuController {

    @Autowired
    RestaurantService restaurantService;

    @Autowired
    MenuService menuService;

    @Autowired
    DishCategoryRepository dishCategoryService;

    @Autowired
    ModelMapper modelMapper;

    //@Cacheable(CURRENTLY_ACTIVE_MENU)
    @GetMapping("/getDailyMenu/{restaurantId}")
    public String getPresentMenuDto(@PathVariable(name = "restaurantId") UUID restaurantId, Model model){

        Restaurant restaurant = restaurantService.get(restaurantId);

        Menu dailyMenu = menuService.getTodaysMenu(restaurantId);

        List<Dish> dishes = dailyMenu.getDishes();

        dishes.forEach(dish -> {dish.getDishDetails(); dish.getDishCategory();});

        RestaurantDTO restaurantDto = modelMapper.map(restaurant, RestaurantDTO.class);

        model.addAttribute("restaurant", restaurantDto);
        model.addAttribute("dailyMenu", dailyMenu);
        model.addAttribute("language", dailyMenu.getDishes().get(0).getDishDetails().entrySet().iterator().next().getKey());
        model.addAttribute("dishCategories", dishCategoryService.findAll(Sort.by(Sort.Direction.ASC, "orderingWeight")));

        return "dailyMenu";

    }
}
