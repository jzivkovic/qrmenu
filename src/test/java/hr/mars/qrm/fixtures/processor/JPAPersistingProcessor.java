package hr.mars.qrm.fixtures.processor;

import br.com.six2six.fixturefactory.processor.Processor;

import javax.persistence.Embeddable;
import javax.persistence.EntityManager;

public class JPAPersistingProcessor implements Processor {
    private EntityManager entityManager;

    public JPAPersistingProcessor(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public void execute(Object result) {
        if (!result.getClass().isAnnotationPresent(Embeddable.class)) {
            this.entityManager.persist(result);
        }
    }
}
