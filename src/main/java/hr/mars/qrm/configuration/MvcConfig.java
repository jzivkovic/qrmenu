package hr.mars.qrm.configuration;

import hr.mars.qrm.api.controller.UIUtil;
import hr.mars.qrm.api.dto.DishDTO;
import hr.mars.qrm.api.dto.DishDetailsDTO;
import hr.mars.qrm.api.dto.MenuDTO;
import hr.mars.qrm.api.dto.RestaurantDTO;
import hr.mars.qrm.api.dto.converter.LocalDateConverter;
import hr.mars.qrm.api.formatter.DayOfWeekFormatter;
import hr.mars.qrm.domain.Dish;
import hr.mars.qrm.domain.DishDetails;
import hr.mars.qrm.domain.Menu;
import hr.mars.qrm.domain.Restaurant;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@EnableWebMvc
@Configuration
public class MvcConfig implements WebMvcConfigurer {

    @Override
    public void addResourceHandlers(
            ResourceHandlerRegistry registry) {

        /*registry.addResourceHandler("/webjars/**")
                .addResourceLocations("/webjars/");
        registry.addResourceHandler("css/**")
                .addResourceLocations("classpath:/static/");*/

        registry.addResourceHandler(
                "/webjars/**",
                "/img/**",
                "/css/**",
                "/js/**",
                "/fonts/**")
                .addResourceLocations(
                        "classpath:/META-INF/resources/webjars/",
                        "classpath:/static/img/",
                        "classpath:/static/css/",
                        "classpath:/static/js/",
                        "classpath:/static/fonts/");
    }

    @Override
    public void addFormatters(FormatterRegistry registry) {

        registry.addFormatter(new DayOfWeekFormatter());
        registry.addConverter(new LocalDateConverter());
    }

    @Bean
    public ModelMapper modelMapper() {
        ModelMapper modelMapper = new ModelMapper();

        modelMapper.typeMap(RestaurantDTO.class, Restaurant.class)
                .addMappings(mapper -> mapper.skip(Restaurant::setMenus))
                .addMappings(mapper -> mapper.skip(Restaurant::setStatus))
        ;

        modelMapper.typeMap(MenuDTO.class, Menu.class)
                .addMappings(mapper -> mapper.skip(Menu::setStatus))
                .addMappings(mapper -> mapper.skip(Menu::setDishes))
        ;

        modelMapper.typeMap(DishDTO.class, Dish.class)
                .addMappings(mapper -> mapper.skip(Dish::setStatus))
                .addMappings(mapper -> mapper.skip(Dish::setDishDetails))
        ;

        modelMapper.addMappings(new PropertyMap<DishDTO, Dish>() {
            @Override
            protected void configure() {
                //this is not java code, this is EDSL
                skip().setDishCategory(null);
                skip().getDishCategory().setId(null);
                skip().getDishCategory().setName(null);
                skip().getDishCategory().setOrderingWeight(0);
                skip().getDishCategory().setUser(null);
            }
        });

        modelMapper.typeMap(DishDetailsDTO.class, DishDetails.class)
                .addMappings(mapper -> mapper.skip(DishDetails::setId))
                .addMappings(mapper -> mapper.skip(DishDetails::setLocale))
        ;

        return modelMapper;
    }

    @Bean
    public static UIUtil getUIUtil(){
        UIUtil IUUtil = new UIUtil();
        return IUUtil;
    }
}
