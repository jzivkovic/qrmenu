package hr.mars.qrm.fixtures;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;
import hr.mars.qrm.domain.Dish;
import hr.mars.qrm.domain.Status;
import hr.mars.qrm.domain.User;

import java.math.BigDecimal;

public class DishTemplateLoader implements TemplateLoader {

    @Override
    public void load() {
        Fixture.of(Dish.class).addTemplate("basic", new Rule(){{
            add("orderingWeight", random(range(10L, 1000L)));
            add("price", uniqueRandom(new BigDecimal("10.27"), new BigDecimal("35.27")));
            add("status", Status.ACTIVE);
            add("user", one(User.class, "basic"));
        }});

        Fixture.of(Dish.class).addTemplate("order_weight_10").inherits("basic", new Rule(){{
            add("orderingWeight", 10);
        }});

        Fixture.of(Dish.class).addTemplate("order_weight_20").inherits("basic", new Rule(){{
            add("orderingWeight", 20);
        }});

        Fixture.of(Dish.class).addTemplate("order_weight_30").inherits("basic", new Rule(){{
            add("orderingWeight", 30);
        }});

        Fixture.of(Dish.class).addTemplate("order_weight_40_inactive").inherits("basic", new Rule(){{
            add("orderingWeight", 40);
            add("status", Status.INACTIVE);
        }});
    }
}
