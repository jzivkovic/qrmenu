package hr.mars.qrm.api.controller;

import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;
import hr.mars.qrm.repository.MenuRepository;
import hr.mars.qrm.service.impl.MenuServiceImpl;
import org.junit.BeforeClass;
import org.mockito.InjectMocks;
import org.mockito.Mock;

public class PresentMenuControllerTest {

    @Mock
    MenuRepository menuRepository;

    @InjectMocks
    MenuServiceImpl menuService;

    @BeforeClass
    public static void loadTemplates(){
        FixtureFactoryLoader.loadTemplates("hr.mars.qrm.fixtures");
    }

}
