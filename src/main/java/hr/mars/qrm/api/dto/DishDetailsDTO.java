package hr.mars.qrm.api.dto;

import javax.validation.constraints.NotNull;
import java.util.Locale;
import java.util.UUID;

public class DishDetailsDTO {

    private UUID id;

    private Locale locale;

    @NotNull
    private String name;

    private String description;

    private boolean defaultDetails;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Locale getLocale() {
        return locale;
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isDefaultDetails() {
        return defaultDetails;
    }

    public void setDefaultDetails(boolean defaultDetails) {
        this.defaultDetails = defaultDetails;
    }
}
