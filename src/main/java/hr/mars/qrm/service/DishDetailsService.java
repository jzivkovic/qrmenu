package hr.mars.qrm.service;

import hr.mars.qrm.domain.DishDetails;

import java.util.List;
import java.util.UUID;

public interface DishDetailsService {

    DishDetails get(UUID id);

    List<DishDetails> getAll();

    DishDetails create();

    DishDetails create(UUID restaurantId);

    void delete(UUID id);

    DishDetails save(DishDetails dishDetails);
}
