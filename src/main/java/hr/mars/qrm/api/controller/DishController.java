package hr.mars.qrm.api.controller;

import hr.mars.qrm.api.dto.DishCategoryDTO;
import hr.mars.qrm.api.dto.DishDTO;
import hr.mars.qrm.api.dto.DishDetailsDTO;
import hr.mars.qrm.domain.Dish;
import hr.mars.qrm.domain.DishCategory;
import hr.mars.qrm.domain.Status;
import hr.mars.qrm.service.DishCategoryService;
import hr.mars.qrm.service.DishService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

@Controller
public class DishController {

    @Autowired
    DishService dishService;

    @Autowired
    DishCategoryService dishCategoryService;

    @Autowired
    ModelMapper modelMapper;

    @RequestMapping(value = "/dish/{dishId}", method = RequestMethod.GET)
    public String get(@PathVariable(name = "dishId") UUID dishId, Model model){

        Dish dish = dishService.get(dishId);
        List<DishCategory> dishCategories = dishCategoryService.getAll();

        DishDTO dishDTO = modelMapper.map(dish, DishDTO.class);

        List<DishCategoryDTO> dishCategoryDTOs = dishCategories.stream().map(dishCategory -> modelMapper.map(dishCategory, DishCategoryDTO.class)).collect(Collectors.toList());

        model.addAttribute("dish", dishDTO);
        model.addAttribute("dishCategories", dishCategoryDTOs);
        model.addAttribute("user", dish.getUser());

        return "dish";

    }

    @RequestMapping(value="/dish", method = RequestMethod.POST)
    public String create(DishDTO dishDTO, Model model){

        Dish dish = new Dish();
        DishCategory category = dishCategoryService.get(dishDTO.getDishCategory().getId());

        if(dish == null ){
            throw new RuntimeException();
        }

        dish.setDishCategory(category);

        modelMapper.map(dishDTO, dish);

        for(Map.Entry<Locale, DishDetailsDTO> entry : dishDTO.getDishDetails().entrySet()){

            dish.getDishDetails().get(entry.getKey()).setName(entry.getValue().getName());
            dish.getDishDetails().get(entry.getKey()).setDescription(entry.getValue().getDescription());
            dish.getDishDetails().get(entry.getKey()).setDefaultDetails(entry.getValue().isDefaultDetails());

        }

        dishService.save(dish);

        return get(dishDTO.getId(), model);
    }

    @RequestMapping(value="/dish/{dishId}", method = RequestMethod.PUT)
    public String save(DishDTO dishDTO, @PathVariable(name = "dishId") UUID dishId, Model model){

        Dish dish = dishService.get(dishId);
        DishCategory category = dishCategoryService.get(dishDTO.getDishCategory().getId());

        if(dish == null ){
            throw new RuntimeException();
        }

        dish.setDishCategory(category);

        modelMapper.map(dishDTO, dish);

        for(Map.Entry<Locale, DishDetailsDTO> entry : dishDTO.getDishDetails().entrySet()){

            dish.getDishDetails().get(entry.getKey()).setName(entry.getValue().getName());
            dish.getDishDetails().get(entry.getKey()).setDescription(entry.getValue().getDescription());
            dish.getDishDetails().get(entry.getKey()).setDefaultDetails(entry.getValue().isDefaultDetails());

        }

        dishService.save(dish);

        return get(dishDTO.getId(), model);
    }

    @RequestMapping(value="/dish/{dishId}", method = RequestMethod.DELETE)
    public String delete(@PathVariable(name = "dishId") UUID dishId, UUID menuId, Model model){

        Dish dish = dishService.get(dishId);

        dish.setStatus(Status.DELETED);

        return "";

    }


}
