package hr.mars.qrm.service.impl;

import hr.mars.qrm.domain.Dish;
import hr.mars.qrm.domain.DishDetails;
import hr.mars.qrm.repository.DishDetailsRepository;
import hr.mars.qrm.repository.DishRepository;
import hr.mars.qrm.service.DishDetailsService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.UUID;

public class DishDetailsServiceImpl implements DishDetailsService {

    @Autowired
    DishDetailsRepository dishDetailsRepository;

    @Autowired
    DishRepository dishRepository;

    @Override
    public DishDetails get(UUID id) {
        return dishDetailsRepository.findById(id).get();
    }

    @Override
    public List<DishDetails> getAll() {
        return dishDetailsRepository.findAll();
    }

    @Override
    public DishDetails create() {

        DishDetails dishDetails = new DishDetails();

        return dishDetails;
    }

    @Override
    public DishDetails create(UUID dishId) {

        Dish dish = dishRepository.findById(dishId).get();

        if(dish == null){
            //TODO ERROR HANDLING
            throw new RuntimeException();
        }

        DishDetails dishDetails = new DishDetails();

        dish.getDishDetails().put(dish.getUser().getLocale(), dishDetails);
        dishDetails.setUser(dish.getUser());

        return dishDetails;
    }


    @Override
    public void delete(UUID id) {
        dishDetailsRepository.deleteById(id);
    }

    @Override
    public DishDetails save(DishDetails dishDetails) {
        return dishDetailsRepository.save(dishDetails);
    }
}
