package hr.mars.qrm.api.dto;

import hr.mars.qrm.domain.Status;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

public class DishDTO {

    private UUID id;

    @NotNull
    private DishCategoryDTO dishCategory;

    private int orderingWeight;

    @NotNull
    private BigDecimal price;

    private Status status;

    private Map<Locale, DishDetailsDTO> dishDetails;

    private UUID currentMenuId;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public DishCategoryDTO getDishCategory() {
        return dishCategory;
    }

    public void setDishCategory(DishCategoryDTO dishCategory) {
        this.dishCategory = dishCategory;
    }

    public int getOrderingWeight() {
        return orderingWeight;
    }

    public void setOrderingWeight(int orderingWeight) {
        this.orderingWeight = orderingWeight;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Map<Locale, DishDetailsDTO> getDishDetails() {
        return dishDetails;
    }

    public void setDishDetails(Map<Locale, DishDetailsDTO> dishDetails) {
        this.dishDetails = dishDetails;
    }

    public UUID getCurrentMenuId() {
        return currentMenuId;
    }

    public void setCurrentMenuId(UUID currentMenuId) {
        this.currentMenuId = currentMenuId;
    }
}
