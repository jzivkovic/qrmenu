package hr.mars.qrm.service.impl;

import hr.mars.qrm.domain.DishCategory;
import hr.mars.qrm.repository.DishCategoryRepository;
import hr.mars.qrm.service.DishCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;

import java.util.List;
import java.util.UUID;

public class DishCategoryServiceImpl implements DishCategoryService {
    @Autowired
    DishCategoryRepository dishCategoryRepository;

    @Override
    public DishCategory get(UUID id) {
        return dishCategoryRepository.findById(id).get();
    }

    @Override
    public List<DishCategory> getAll() {
        return dishCategoryRepository.findAll();
    }

    @Override
    public List<DishCategory> getAll(Sort sort) {
        return dishCategoryRepository.findAll(sort);
    }

    @Override
    public DishCategory create() {

        DishCategory dishCategory = new DishCategory();

        return dishCategory;
    }

    @Override
    public void delete(UUID id) {
        dishCategoryRepository.deleteById(id);
    }

    @Override
    public DishCategory save(DishCategory dishCategory) {
        return dishCategoryRepository.save(dishCategory);
    }
}
