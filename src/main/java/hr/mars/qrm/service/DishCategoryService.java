package hr.mars.qrm.service;

import hr.mars.qrm.domain.DishCategory;
import org.springframework.data.domain.Sort;

import java.util.List;
import java.util.UUID;

public interface DishCategoryService {

    DishCategory get(UUID id);

    List<DishCategory> getAll();

    List<DishCategory> getAll(Sort sort);

    DishCategory create();

    void delete(UUID id);

    DishCategory save(DishCategory dishCategory);
    
}
