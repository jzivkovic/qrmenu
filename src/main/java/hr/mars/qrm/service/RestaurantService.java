package hr.mars.qrm.service;

import hr.mars.qrm.domain.Restaurant;

import java.util.List;
import java.util.UUID;

public interface RestaurantService {

    Restaurant get(UUID id);

    List<Restaurant> getAll();

    List<Restaurant> getAllForUser(UUID userId);

    Restaurant create();

    Restaurant create(UUID userId);

    void delete(UUID id);

    Restaurant save(Restaurant restaurant);
}
