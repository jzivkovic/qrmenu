package hr.mars.qrm.domain;

public enum Status {
    ACTIVE, INACTIVE, DELETED
}
