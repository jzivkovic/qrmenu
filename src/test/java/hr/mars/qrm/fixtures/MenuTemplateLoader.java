package hr.mars.qrm.fixtures;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;
import hr.mars.qrm.domain.*;

import java.time.DayOfWeek;
import java.time.LocalDate;

import static hr.mars.qrm.fixtures.function.Functions.arrayList;

public class MenuTemplateLoader implements TemplateLoader {

    @Override
    public void load() {
        Fixture.of(Menu.class).addTemplate("basic", new Rule(){{
            add("status", Status.ACTIVE);
            add("name", "menu " + name());
            add("user", one(User.class,"basic"));
            add("restaurant", one(Restaurant.class, "basic"));
            add("dishes", arrayList(Dish.class));
        }});

        Fixture.of(Menu.class).addTemplate("valid_on_date_today").inherits("basic",  new Rule(){{
            add("validOnDate", LocalDate.now());
        }});

        Fixture.of(Menu.class).addTemplate("valid_on_day_friday").inherits("basic",  new Rule(){{
            add("validOnDayOfWeek", DayOfWeek.FRIDAY);
        }});

        Fixture.of(Menu.class).addTemplate("valid_endless").inherits("basic",  new Rule(){{
            add("validEndless", true);
        }});
    }
}
