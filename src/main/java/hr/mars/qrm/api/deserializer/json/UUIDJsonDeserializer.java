package hr.mars.qrm.api.deserializer.json;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.node.TextNode;
import org.springframework.boot.jackson.JsonComponent;

import java.io.IOException;
import java.util.UUID;

@JsonComponent
public class UUIDJsonDeserializer extends JsonDeserializer<UUID> {

    @Override
    public UUID deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {

        TreeNode treeNode = jsonParser.getCodec().readTree(jsonParser);
        TextNode textNode = (TextNode) treeNode.get(0);

        if(textNode == null){
            return null;
        }

        UUID uuid = null;
        try {
            uuid = UUID.fromString(textNode.textValue());
        }catch (IllegalArgumentException exception){
            exception.printStackTrace();
        }

        return uuid;
    }
}
