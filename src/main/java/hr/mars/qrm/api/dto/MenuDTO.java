package hr.mars.qrm.api.dto;

import hr.mars.qrm.domain.Status;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

public class MenuDTO {

    private UUID id;

    private String name;

    private LocalDate validOnDate;

    private DayOfWeek validOnDayOfWeek;

    private Boolean validAlways;

    private Status status;

    private List<DishDTO> dishes;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getValidOnDate() {
        return validOnDate;
    }

    public void setValidOnDate(LocalDate validOnDate) {
        this.validOnDate = validOnDate;
    }

    public DayOfWeek getValidOnDayOfWeek() {
        return validOnDayOfWeek;
    }

    public void setValidOnDayOfWeek(DayOfWeek validOnDayOfWeek) {
        this.validOnDayOfWeek = validOnDayOfWeek;
    }

    public Boolean getValidAlways() {
        return validAlways;
    }

    public void setValidAlways(Boolean validAlways) {
        this.validAlways = validAlways;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public List<DishDTO> getDishes() {
        return dishes;
    }

    public void setDishes(List<DishDTO> dishes) {
        this.dishes = dishes;
    }
}
