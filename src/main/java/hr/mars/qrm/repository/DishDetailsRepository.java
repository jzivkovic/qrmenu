package hr.mars.qrm.repository;

import hr.mars.qrm.domain.DishDetails;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RepositoryRestResource
@Repository
public interface DishDetailsRepository extends JpaRepository<DishDetails, UUID>{
    @Override
    @RestResource(exported = true)
    List<DishDetails> findAll();

    @Override
    @RestResource(exported = true)
    List<DishDetails> findAll(Sort sort);

    @Override
    @RestResource(exported = true)
    Page<DishDetails> findAll(Pageable pageable);

    @Override
    @RestResource(exported = true)
    Optional<DishDetails> findById(UUID uuid);

    @Override
    @RestResource(exported = true)
    @PreAuthorize("#DD.userId == principal.userId or hasRole('ADMIN')")
    DishDetails save(@Param("DD") DishDetails s);

    @Override
    @RestResource(exported = true)
    @PreAuthorize("#DD.userId = principal.userId or hasRole('ADMIN')")
    void delete(@Param("DD") DishDetails DishDetails);
}
