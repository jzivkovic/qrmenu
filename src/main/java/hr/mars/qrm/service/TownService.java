package hr.mars.qrm.service;

import hr.mars.qrm.domain.Town;

import java.util.List;

public interface TownService {

    List<Town> findAll();
}
