package hr.mars.qrm.service;

import hr.mars.qrm.domain.Menu;

import java.util.List;
import java.util.UUID;

public interface MenuService {

    Menu getTodaysMenu(UUID restaurantId);

    Menu get(UUID id);

    List<Menu> getAll();

    Menu create();

    Menu create(UUID restaurantId);

    void delete(UUID id);

    Menu save(Menu menu);
}
