package hr.mars.qrm.api.controller;

import hr.mars.qrm.api.dto.MenuDTO;
import hr.mars.qrm.api.dto.RestaurantDTO;
import hr.mars.qrm.domain.Menu;
import hr.mars.qrm.domain.Restaurant;
import hr.mars.qrm.domain.Status;
import hr.mars.qrm.domain.Town;
import hr.mars.qrm.service.MenuService;
import hr.mars.qrm.service.RestaurantService;
import hr.mars.qrm.service.TownService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;
import java.util.UUID;

@Controller
public class RestaurantController {

    @Autowired
    RestaurantService restaurantService;

    @Autowired
    TownService townService;

    @Autowired
    MenuService menuService;

    @Autowired
    ModelMapper modelMapper;

    @Autowired
    UIUtil UIUtilInstance;

    //@Cacheable(CURRENTLY_ACTIVE_MENU)
    @GetMapping("/restaurant/{restaurantId}")
    public String get(@PathVariable(name = "restaurantId") UUID restaurantId, Model model){

        Restaurant restaurant = restaurantService.get(restaurantId);
        List<Town> towns = townService.findAll();

        RestaurantDTO restaurantDTO = modelMapper.map(restaurant, RestaurantDTO.class);

        model.addAttribute("restaurant", restaurantDTO);
        model.addAttribute( "towns", towns);
        model.addAttribute("UIUtil", UIUtilInstance);

        return "restaurant";

    }

    @RequestMapping(value="/restaurant/save", method = RequestMethod.POST)
    public String save( RestaurantDTO restaurantDTO, Model model){

        Restaurant restaurant = restaurantService.get(restaurantDTO.getId());

        if(restaurant == null ){
            throw new RuntimeException();
        }

        modelMapper.map(restaurantDTO, restaurant);

        restaurantService.save(restaurant);

        return get(restaurantDTO.getId(), model);
    }

    @RequestMapping(value="/newMenu/{restaurantId}", method=RequestMethod.GET)
    public String createMenu(@PathVariable(name = "restaurantId") UUID restaurantId, Model model){

        Menu menu = new Menu();
        Restaurant restaurant = restaurantService.get(restaurantId);

        menu.setRestaurant(restaurant);
        menu.setStatus(Status.ACTIVE);
        menu.setUser(restaurant.getUser());


        menuService.save(menu);

        MenuDTO menuDTO = modelMapper.map(menu, MenuDTO.class);

        model.addAttribute("menu", menuDTO);
        model.addAttribute("user", menu.getUser());

        return "menu";

    }
}
