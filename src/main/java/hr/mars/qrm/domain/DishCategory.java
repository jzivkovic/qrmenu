package hr.mars.qrm.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Entity
@Table(name="dish_categories")
public class DishCategory {

    public DishCategory() {
    }

    @Id
    @GeneratedValue
    private UUID id;

    @ManyToOne
    private User user;

    @NotNull
    private String name;

    private int orderingWeight;

    private DishCategory(Builder builder) {
        setId(builder.id);
        setUser(builder.user);
        setName(builder.name);
        setOrderingWeight(builder.orderingWeight);
    }

    public static Builder newBuilder() {
        return new Builder();
    }


    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getOrderingWeight() {
        return orderingWeight;
    }

    public void setOrderingWeight(int orderingWeight) {
        this.orderingWeight = orderingWeight;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }


    public static final class Builder {
        private UUID id;
        private User user;
        private @NotNull String name;
        private int orderingWeight;

        private Builder() {
        }

        public Builder withId(UUID val) {
            id = val;
            return this;
        }

        public Builder withUser(User val) {
            user = val;
            return this;
        }

        public Builder withName(@NotNull String val) {
            name = val;
            return this;
        }

        public Builder withOrderingWeight(int val) {
            orderingWeight = val;
            return this;
        }

        public DishCategory build() {
            return new DishCategory(this);
        }
    }
}

