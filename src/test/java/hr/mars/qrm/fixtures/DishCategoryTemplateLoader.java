package hr.mars.qrm.fixtures;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;
import hr.mars.qrm.domain.DishCategory;
import hr.mars.qrm.domain.User;

public class DishCategoryTemplateLoader implements TemplateLoader {

    @Override
    public void load() {
        Fixture.of(DishCategory.class).addTemplate("basic", new Rule(){{
            add("orderingWeight", random(range(10L, 1000L)));
            add("name", uniqueRandom("Appetizers", "Main Dish", "Desert", "Drinks", "Wine list"));
            add("user", one(User.class, "basic"));
        }});

        Fixture.of(DishCategory.class).addTemplate("ordering_weight_10").inherits("basic",new Rule(){{
            add("orderingWeight", 10);
        }});

        Fixture.of(DishCategory.class).addTemplate("ordering_weight_20").inherits("basic",new Rule(){{
            add("orderingWeight", 20);
        }});

        Fixture.of(DishCategory.class).addTemplate("ordering_weight_30").inherits("basic",new Rule(){{
            add("orderingWeight", 30);
        }});
    }

}
