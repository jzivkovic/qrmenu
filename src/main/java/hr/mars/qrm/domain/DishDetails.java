package hr.mars.qrm.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Locale;
import java.util.UUID;

@Entity
@Table(name="dish_details")
public class DishDetails
{

    public DishDetails() {
    }

    @Id
    @GeneratedValue
    private UUID id;

    @ManyToOne
    @NotNull
    private User user;

    @NotNull
    private Locale locale;

    @ManyToOne
    @JoinColumn(name = "dish_id")
    @NotNull
    @JsonIgnore
    private Dish dish;

    @NotNull
    private String name;

    private String description;

    private boolean defaultDetails;

    private DishDetails(Builder builder) {
        setId(builder.id);
        setUser(builder.user);
        setLocale(builder.locale);
        setDish(builder.dish);
        setName(builder.name);
        setDescription(builder.description);
        setDefaultDetails(builder.defaultDetails);
    }

    public static Builder newBuilder() {
        return new Builder();
    }


    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Locale getLocale() {
        return locale;
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    public Dish getDish() {
        return dish;
    }

    public void setDish(Dish dish) {
        this.dish = dish;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isDefaultDetails() {
        return defaultDetails;
    }

    public void setDefaultDetails(boolean defaultDetails) {
        this.defaultDetails = defaultDetails;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }


    public static final class Builder {
        private UUID id;
        private @NotNull User user;
        private @NotNull Locale locale;
        private @NotNull Dish dish;
        private @NotNull String name;
        private String description;
        private boolean defaultDetails;

        private Builder() {
        }

        public Builder withId(UUID val) {
            id = val;
            return this;
        }

        public Builder withUser(@NotNull User val) {
            user = val;
            return this;
        }

        public Builder withLanguage(@NotNull Locale val) {
            locale = val;
            return this;
        }

        public Builder withDish(@NotNull Dish val) {
            dish = val;
            return this;
        }

        public Builder withName(@NotNull String val) {
            name = val;
            return this;
        }

        public Builder withDescription(String val) {
            description = val;
            return this;
        }

        public Builder withDefaultDetails(boolean val) {
            defaultDetails = val;
            return this;
        }

        public DishDetails build() {
            return new DishDetails(this);
        }
    }
}