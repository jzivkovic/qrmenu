package hr.mars.qrm.repository;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;
import hr.mars.qrm.domain.Dish;
import hr.mars.qrm.domain.DishCategory;
import hr.mars.qrm.domain.Menu;
import hr.mars.qrm.domain.Status;
import hr.mars.qrm.fixtures.processor.JPAPersistingProcessor;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.util.AssertionErrors.assertTrue;

@RunWith(SpringRunner.class)
@DataJpaTest
public class DishRepositoryTest {

    @Autowired
    public JdbcTemplate jdbcTemplate;

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    DishRepository dishRepository;


    JPAPersistingProcessor jpaProcessor;

    @BeforeClass
    public static void loadTemplates(){
        FixtureFactoryLoader.loadTemplates("hr.mars.qrm.fixtures");
    }

    @Before
    public void init() {
        jdbcTemplate.execute("delete FROM dish_details;\n" +
                "delete from menus_dishes_cross;\n" +
                "delete from dishes;\n" +
                "delete from menus;\n" +
                "delete from restaurants;\n" +
                "delete dish_categories;\n"+
                "delete from users_roles_cross;\n"+
                "delete from users;\n");

        jpaProcessor = new JPAPersistingProcessor(entityManager.getEntityManager());
    }

    @Test
    public void findByMenuIdOrdered_DishesWithoutCategory_ReturnsOrderedDishes(){

        List<Dish> dishes1 = Fixture.from(Dish.class).uses(jpaProcessor).gimme(4,"order_weight_10","order_weight_20", "order_weight_30", "order_weight_40_inactive");
        List<Dish> dishes2 = Fixture.from(Dish.class).uses(jpaProcessor).gimme(4,"order_weight_10","order_weight_20", "order_weight_30", "order_weight_40_inactive");
        List<Menu> menus = Fixture.from(Menu.class).uses(jpaProcessor).gimme(2,"basic");

        menus.get(0).getDishes().addAll(dishes1);
        menus.get(1).getDishes().addAll(dishes2);

        List<Dish> orderedDishes = dishRepository.findByMenuIdAndStatusOrderByCategory_OrderingWeightAndDish_OrderingWeight(menus.get(0).getId(), Status.ACTIVE.name());

        assertTrue("Incorrect number of Dish entities", orderedDishes.size() == 3);
        assertEquals("Wrong dish order", 30, orderedDishes.get(0).getOrderingWeight());
        assertEquals("Wrong dish order", 20, orderedDishes.get(1).getOrderingWeight());
        assertEquals("Wrong dish order", 10, orderedDishes.get(2).getOrderingWeight());
    }

    @Test
    public void findByMenuIdOrdered_DishesWithCategory_ReturnsOrderedDishes(){

        List<DishCategory> categories = Fixture.from(DishCategory.class).uses(jpaProcessor).gimme(2,"ordering_weight_20", "ordering_weight_30");
        List<Dish> dishes1 = Fixture.from(Dish.class).uses(jpaProcessor).gimme(6,"order_weight_10", "order_weight_20", "order_weight_10", "order_weight_20", "order_weight_30", "order_weight_40_inactive");
        List<Dish> dishes2 = Fixture.from(Dish.class).uses(jpaProcessor).gimme(6,"order_weight_10", "order_weight_20", "order_weight_10", "order_weight_20", "order_weight_30", "order_weight_40_inactive");
        List<Menu> menus = Fixture.from(Menu.class).uses(jpaProcessor).gimme(2,"basic");

        for (int i = 0; i < categories.size(); i++) {
            dishes1.get(0+i*2).setDishCategory(categories.get(i));
            dishes1.get(1+i*2).setDishCategory(categories.get(i));
        }

        menus.get(0).getDishes().addAll(dishes1);
        menus.get(1).getDishes().addAll(dishes2);

        List<Dish> orderedDishes = dishRepository.findByMenuIdAndStatusOrderByCategory_OrderingWeightAndDish_OrderingWeight(menus.get(0).getId(), Status.ACTIVE.name());

        assertTrue("Incorrect number of Dish entities", orderedDishes.size() == 5);
        assertTrue("Wrong dish order", dishOrderingWeight(orderedDishes,0) == 20 && categoryOrderingWeight(orderedDishes,0) == 30);
        assertTrue("Wrong dish order", dishOrderingWeight(orderedDishes,1) == 10 && categoryOrderingWeight(orderedDishes,1) == 30);
        assertTrue("Wrong dish order", dishOrderingWeight(orderedDishes,2) == 20 && categoryOrderingWeight(orderedDishes,2) == 20);
        assertTrue("Wrong dish order", dishOrderingWeight(orderedDishes,3) == 10 && categoryOrderingWeight(orderedDishes,3) == 20);
        assertTrue("Wrong dish order", dishOrderingWeight(orderedDishes,4) == 30 && orderedDishes.get(4).getDishCategory() == null);
    }

    private int dishOrderingWeight(List<Dish> dishes, int index){
        return dishes.get(index).getOrderingWeight();
    }

    private int categoryOrderingWeight(List<Dish> dishes, int index){
        return dishes.get(index).getDishCategory().getOrderingWeight();
    }
}
