package hr.mars.qrm.fixtures;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;
import hr.mars.qrm.domain.Dish;
import hr.mars.qrm.domain.DishDetails;
import hr.mars.qrm.domain.Status;
import hr.mars.qrm.domain.User;

import java.math.BigDecimal;

public class DishDetailsTemplateLoader implements TemplateLoader {


    @Override
    public void load() {
        Fixture.of(DishDetails.class).addTemplate("basic_user1", new Rule(){{
            add("language", random(range(10L, 1000L)));
            add("name", uniqueRandom(new BigDecimal("10.27"), new BigDecimal("35.27")));
            add("description", Status.ACTIVE);
            add("user", one(User.class, "user1"));
            add("dish", one(Dish.class, "basic_user1"));
        }});

    }
}
