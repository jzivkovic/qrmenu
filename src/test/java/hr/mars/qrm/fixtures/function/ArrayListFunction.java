package hr.mars.qrm.fixtures.function;

import br.com.six2six.fixturefactory.function.AtomicFunction;

import java.util.ArrayList;

public class ArrayListFunction <T> implements AtomicFunction{

    @Override
    public ArrayList<T> generateValue() {
        return new ArrayList<T>();
    }
}
