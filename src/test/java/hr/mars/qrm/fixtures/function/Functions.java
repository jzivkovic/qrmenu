package hr.mars.qrm.fixtures.function;

import br.com.six2six.fixturefactory.function.Function;

import java.util.HashMap;
import java.util.Map;

public class Functions {

    static Map<Object, Function> arrayListFunctions = new HashMap<>();

    public static <T> Function arrayList(T type){

        if(arrayListFunctions.get(type)==null){
            arrayListFunctions.put(type, new ArrayListFunction<T>());
        }

        return arrayListFunctions.get(type);
    }
}
