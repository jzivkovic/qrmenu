package hr.mars.qrm.service;

import hr.mars.qrm.domain.Dish;

import java.util.List;
import java.util.UUID;

public interface DishService {

    Dish get(UUID id);

    List<Dish> getAll();

    Dish create();

    Dish create(UUID restaurantId);

    void delete(UUID id);

    Dish save(Dish dish);
}
