package hr.mars.qrm.repository;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;
import hr.mars.qrm.domain.Menu;
import hr.mars.qrm.domain.Restaurant;
import hr.mars.qrm.domain.Status;
import hr.mars.qrm.fixtures.processor.JPAPersistingProcessor;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.springframework.test.util.AssertionErrors.assertTrue;

@RunWith(SpringRunner.class)
@DataJpaTest
public class MenuRepositoryTest {

    @Autowired
    public JdbcTemplate jdbcTemplate;

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private MenuRepository menuRepository;

    JPAPersistingProcessor hibernateProcessor;

    @BeforeClass
    public static void loadTemplates(){
        FixtureFactoryLoader.loadTemplates("hr.mars.qrm.fixtures");
    }

    @Before
    public void init() {
        jdbcTemplate.execute("delete FROM dish_details;\n" +
                "delete from menus_dishes_cross;\n" +
                "delete from dishes;\n" +
                "delete from menus;\n" +
                "delete from restaurants;\n" +
                "delete dish_categories;\n"+
                "delete from users_roles_cross;\n"+
                "delete from users;\n");

        hibernateProcessor = new JPAPersistingProcessor(entityManager.getEntityManager());
    }

    @Test
    public void findByRestaurantId_TwoRestaurantsInDB_ReturnsCorrectRestaurant(){
        List<Menu> menus = Fixture.from(Menu.class).uses(hibernateProcessor).gimme(3,"basic");
        List<Restaurant> restaurants = Fixture.from(Restaurant.class).uses(hibernateProcessor).gimme(3,"basic");

        for (int i = 0; i < menus.size(); i++) {
            menus.get(i).setRestaurant(restaurants.get(i));
            restaurants.get(i).getMenus().add(menus.get(i));
        }

        List<Menu> returnedMenues = menuRepository.findByRestaurantId(restaurants.get(0).getId());

        assertTrue("Unexpected number of menus", returnedMenues.size()==1);
        assertEquals("Wrong menu returned", returnedMenues.get(0), menus.get(0));
    }

    @Test
    public void findByRestaurantIdAndStatus_activeStatus_activeMenus(){
        List<Menu> menus = Fixture.from(Menu.class).uses(hibernateProcessor).gimme(9,"basic");
        List<Restaurant> restaurants = Fixture.from(Restaurant.class).uses(hibernateProcessor).gimme(3,"basic");

        for (int i = 0; i < menus.size(); i++) {
            menus.get(i).setRestaurant(restaurants.get(i%3));
            menus.get(i).setStatus( i%2 == 0 ? Status.ACTIVE : Status.INACTIVE);
            restaurants.get(i%3).getMenus().add(menus.get(i));
        }

        List<Menu> returnedMenus = menuRepository.findByRestaurantIdAndStatus(restaurants.get(0).getId(), Status.ACTIVE);

        assertTrue("Unexpected number of menus", returnedMenus.size()==2);
        assertArrayEquals("Wrong menus returned",
                restaurants.get(0).getMenus().stream().filter(menu -> menu.getStatus() == Status.ACTIVE).toArray(),
                returnedMenus.toArray());
    }

    @Test
    public void findByRestaurantIdAndStatus_inactiveStatus_inactiveMenu(){
        List<Menu> menus = Fixture.from(Menu.class).uses(hibernateProcessor).gimme(9,"basic");
        List<Restaurant> restaurants = Fixture.from(Restaurant.class).uses(hibernateProcessor).gimme(3,"basic");

        for (int i = 0; i < menus.size(); i++) {
            menus.get(i).setRestaurant(restaurants.get(i%3));
            menus.get(i).setStatus( i%2 == 0 ? Status.ACTIVE : Status.INACTIVE);
            restaurants.get(i%3).getMenus().add(menus.get(i));
        }

        List<Menu> returnedMenus = menuRepository.findByRestaurantIdAndStatus(restaurants.get(0).getId(), Status.INACTIVE);

        assertEquals("Unexpected number of menus", 1, returnedMenus.size());
        assertArrayEquals("Wrong menus returned",
                restaurants.get(0).getMenus().stream().filter(menu -> menu.getStatus() == Status.INACTIVE).toArray(),
                returnedMenus.toArray());
    }
}
