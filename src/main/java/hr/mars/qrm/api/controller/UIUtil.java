package hr.mars.qrm.api.controller;

import hr.mars.qrm.domain.Status;

import java.util.HashMap;
import java.util.Map;

public class UIUtil {

    private static Map<Status, String> restaurantStatusToBootstrapClassMapping = new HashMap<>();
    private static UIUtil instance;

    static{

        restaurantStatusToBootstrapClassMapping.put(Status.ACTIVE, "list-group-item-default");
        restaurantStatusToBootstrapClassMapping.put(Status.INACTIVE, "list-group-item-danger");

    }

    public static String getBootstrapClassForRestaurantStatus(Status status){

        String bootstrapClass = restaurantStatusToBootstrapClassMapping.get(status);

        if(bootstrapClass == null){
            bootstrapClass =  restaurantStatusToBootstrapClassMapping.get(Status.ACTIVE);
        }

        return bootstrapClass;
    }
}
