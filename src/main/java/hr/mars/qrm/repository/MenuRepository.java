package hr.mars.qrm.repository;

import hr.mars.qrm.domain.Menu;
import hr.mars.qrm.domain.Status;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RepositoryRestResource
@Repository
public interface MenuRepository extends JpaRepository<Menu,UUID>{
    
    @Override
    List<Menu> findAll();

    @Override
    List<Menu> findAll(Sort sort);

    @Override
    Page<Menu> findAll(Pageable pageable);

    List<Menu> findByRestaurantId(UUID restourantId);

    List<Menu> findByRestaurantIdAndStatus(UUID restaurantId, Status status);

    @RestResource(rel = "findByRestaurantIdAndStatusSorted", path = "findByRestaurantIdAndStatusSorted")
    List<Menu> findByRestaurantIdAndStatus(UUID restaurantId, Status status, Sort sort);

    @Override
    Optional<Menu> findById(UUID uuid);

    @Override
    //@PreAuthorize("#M.user == principal.user or hasRole('ADMIN')" )
    Menu save(@Param("M") Menu s);

    @Override
    //@PreAuthorize("#M.user = principal.user or hasRole('ADMIN')")
    void delete(@Param("M") Menu Menu);

}
