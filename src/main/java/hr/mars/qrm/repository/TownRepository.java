package hr.mars.qrm.repository;

import hr.mars.qrm.domain.Town;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RepositoryRestResource
@Repository
public interface TownRepository extends JpaRepository<Town,UUID>{

    @Override
    @RestResource(exported = true)
    List<Town> findAll();

    @Override
    @RestResource(exported = true)
    List<Town> findAll(Sort sort);

    @Override
    @RestResource(exported = true)
    List<Town> findAllById(Iterable<UUID> iterable);

    @Override
    @RestResource(exported = true)
    Page<Town> findAll(Pageable pageable);

    @Override
    @RestResource(exported = true)
    Optional<Town> findById(UUID uuid);
}