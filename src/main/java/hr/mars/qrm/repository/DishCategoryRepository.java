package hr.mars.qrm.repository;

import hr.mars.qrm.domain.DishCategory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RepositoryRestResource
@Repository
public interface DishCategoryRepository extends JpaRepository<DishCategory, UUID>{

    @Override
    @RestResource(exported = true)
    List<DishCategory> findAll();

    @Override
    @RestResource(exported = true)
    List<DishCategory> findAll(Sort sort);

    @Override
    @RestResource(exported = true)
    List<DishCategory> findAllById(Iterable<UUID> iterable);

    @Override
    @RestResource(exported = true)
    Page<DishCategory> findAll(Pageable pageable);

    @Override
    @RestResource(exported = true)
    Optional<DishCategory> findById(UUID uuid);

    @Override
    @RestResource(exported = true)
    boolean existsById(UUID uuid);

}
