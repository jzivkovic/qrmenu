package hr.mars.qrm.service.impl;

import hr.mars.qrm.domain.Dish;
import hr.mars.qrm.domain.Menu;
import hr.mars.qrm.domain.Restaurant;
import hr.mars.qrm.domain.Status;
import hr.mars.qrm.repository.DishRepository;
import hr.mars.qrm.repository.RestaurantRepository;
import hr.mars.qrm.service.DishService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.UUID;

public class DishServiceImpl implements DishService{

    @Autowired
    DishRepository dishRepository;

    @Autowired
    RestaurantRepository restaurantRepository;

    @Override
    public Dish get(UUID id) {
        return dishRepository.findById(id).get();
    }

    @Override
    public List<Dish> getAll() {
        return dishRepository.findAll();
    }

    @Override
    public Dish create() {

        Dish dish = new Dish();

        return dish;
    }

    @Override
    public Dish create(UUID restaurantId) {

        Restaurant restaurant = restaurantRepository.findById(restaurantId).get();

        if(restaurant == null){
            //TODO ERROR HANDLING
            throw new RuntimeException();
        }

        Menu menu = restaurant.getMenus().iterator().next();
        if(menu == null){
            //TODO ERROR HANDLING
            throw new RuntimeException();
        }

        Dish dish = new Dish();

        menu.getDishes().add(dish);
        dish.setUser(restaurant.getUser());
        dish.setStatus(Status.INACTIVE);

        return dish;
    }

    @Override
    public void delete(UUID id) {
        dishRepository.deleteById(id);
    }

    @Override
    public Dish save(Dish dish) {
        return dishRepository.save(dish);
    }
}
