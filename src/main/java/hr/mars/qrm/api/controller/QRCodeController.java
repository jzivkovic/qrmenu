package hr.mars.qrm.api.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class QRCodeController {

    @RequestMapping("/")
    public String helloWorld(){
        return "My first Spring Boot app";
    }
}
