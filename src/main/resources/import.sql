INSERT INTO users (id, enabled, password, username, locale) VALUES ('0d949a24-e44f-4cf8-ab58-40894d1ca756', true, '$2a$10$ge/U2qtCksK42Ex8Bds5ReHE48Ph48zgYWvGLdp2mD982x60aDPia', 'test', 'en');
INSERT INTO users (id, enabled, password, username, locale) VALUES ('b579c480-db88-4188-a6e7-175af19ad1cc', true, '$2a$10$pGSN7hrOIdfKEHMH1W5LfuxM04tBhOOXbvoqzhrvo/ZK/NrzGd3pW', 'test 2', 'en');
INSERT INTO users (id, enabled, password, username, locale) VALUES ('9f150368-0210-4cb1-bfcc-ec07336f7bc3', true, '$2a$10$pGSN7hrOIdfKEHMH1W5LfuxM04tBhOOXbvoqzhrvo/ZK/NrzGd3pW', 'admin', 'en');

INSERT INTO roles (id, name) VALUES ('d7465b78-c0a6-45e4-94da-4b2771f1971e', 'ROLE_USER');
INSERT INTO roles (id, name) VALUES ('d7465b78-c0a6-45e4-94da-4b2771f1971f', 'ROLE_TEST_USER');
INSERT INTO roles (id, name) VALUES ('eac18614-d354-46db-a5db-366c46b916bc', 'ROLE_ADMIN');

INSERT INTO users_roles_cross (user_id, role_id) VALUES ('0d949a24-e44f-4cf8-ab58-40894d1ca756', 'd7465b78-c0a6-45e4-94da-4b2771f1971e');
INSERT INTO users_roles_cross (user_id, role_id) VALUES ('b579c480-db88-4188-a6e7-175af19ad1cc', 'd7465b78-c0a6-45e4-94da-4b2771f1971f');
INSERT INTO users_roles_cross (user_id, role_id) VALUES ('9f150368-0210-4cb1-bfcc-ec07336f7bc3', 'eac18614-d354-46db-a5db-366c46b916bc');

INSERT INTO towns (id, name, zipcode) VALUES ('c4fb846a-0434-439e-d896-7fcb5c9b876b', 'Pula', 52100);
INSERT INTO towns (id, name, zipcode) VALUES ('c4fb846a-0434-439e-d896-7fcb5c9b889b', 'New York', 10001);
INSERT INTO towns (id, name, zipcode) VALUES ('c4fb846a-0434-439e-d896-7fcb5c9b878b', 'Novosibirsk', 630001);

--test user restourant
INSERT INTO restaurants (id, address, x_coordinate, y_coordinate, name, status, user_id, town_id, phone_number, opening_hours) VALUES ('ef806545-1f5f-4d90-860f-2b9e2042e36a', 'Istr cul 23', 12, 23, 'Mažuran', 'ACTIVE', 'b579c480-db88-4188-a6e7-175af19ad1cc', 'c4fb846a-0434-439e-d896-7fcb5c9b876b', '+385 99 783 2175', 'Breakfast - Monday to Saturday 8:00 - 11:00 \\n Dinner - Monday to Saturday 19:00 - 22:00');
--test 2 user restaurant
INSERT INTO restaurants (id, address, x_coordinate, y_coordinate, name, status, user_id, town_id, phone_number) VALUES ('88319661-f437-48cf-97bf-b70a597ef5f9', 'Emova ulica 12', 12678, 78, 'Backyard Restaurant', 'ACTIVE', 'b579c480-db88-4188-a6e7-175af19ad1cc', 'c4fb846a-0434-439e-d896-7fcb5c9b876b', '+385 98 5789 898');

INSERT INTO menus (id, valid_on_day_of_week, valid_always, name, status, valid_on_date, restaurant_id, user_id) VALUES ('3a66b9e3-925a-4c81-adb6-5988d22dda8a', NULL, true, 'Bivol menu', 'ACTIVE', NULL, '88319661-f437-48cf-97bf-b70a597ef5f9', 'b579c480-db88-4188-a6e7-175af19ad1cc');
INSERT INTO menus (id, valid_on_day_of_week, valid_always, name, status, valid_on_date, restaurant_id, user_id) VALUES ('293f2a14-6672-4111-bc48-ccf66a7eedb1', NULL, true, 'Mažuran menu', 'ACTIVE', NULL, 'ef806545-1f5f-4d90-860f-2b9e2042e36a', '0d949a24-e44f-4cf8-ab58-40894d1ca756');

INSERT INTO languages (id, name, code) VALUES ('503bcfec-b2ce-11e9-a2a3-2a2ae2dbcce4', 'English', 'EN');
INSERT INTO languages (id, name, code) VALUES ('503bd2c6-b2ce-11e9-a2a3-2a2ae2dbcce4', 'German', 'DE');
INSERT INTO languages (id, name, code) VALUES ('503bd668-b2ce-11e9-a2a3-2a2ae2dbcce4', 'Italian', 'IT');

INSERT INTO dish_categories (id, user_id, name, ordering_weight) VALUES ('8ed23882-853d-483a-98cf-40ca1be99212', 'b579c480-db88-4188-a6e7-175af19ad1cc', 'Main dish' , 3);
INSERT INTO dish_categories (id, user_id, name, ordering_weight) VALUES ('0a52455b-2290-4604-8f6f-b8fd791340b8', 'b579c480-db88-4188-a6e7-175af19ad1cc', 'Appetizers' , 1);
INSERT INTO dish_categories (id, user_id, name, ordering_weight) VALUES ('c4fb846a-0434-439e-a906-7fcb5c9b876b', 'b579c480-db88-4188-a6e7-175af19ad1cc', 'Deserts' , 7);


INSERT INTO dishes (id, ordering_weight, price, status, dish_category_id, user_id) VALUES ('88319661-f437-48cf-97bf-b70a597ef5f9', 27, 1855.00, 'ACTIVE', '8ed23882-853d-483a-98cf-40ca1be99212', 'b579c480-db88-4188-a6e7-175af19ad1cc');
INSERT INTO dish_details (id, default_details, description, name, dish_id, locale, user_id) VALUES ('503bd7bc-b2ce-11e9-a2a3-2a2ae2dbcce4', true, 'Carrot soup with onion rings ', 'Fried eggs', '88319661-f437-48cf-97bf-b70a597ef5f9', 'en','b579c480-db88-4188-a6e7-175af19ad1cc');
INSERT INTO menus_dishes_cross (menu_id, dish_id) values ('3a66b9e3-925a-4c81-adb6-5988d22dda8a', '88319661-f437-48cf-97bf-b70a597ef5f9');

INSERT INTO dishes (id, ordering_weight, price, status, dish_category_id, user_id) VALUES ('3365528c-efeb-4f25-a8c7-032e368e064a', 1, 25, 'ACTIVE', '0a52455b-2290-4604-8f6f-b8fd791340b8', 'b579c480-db88-4188-a6e7-175af19ad1cc');
INSERT INTO dish_details (id, default_details, description, name, dish_id, locale, user_id) VALUES ('503bd7bc-b2ce-2727-a2a3-2a2ae2dbcce4', true, 'Carrot soup with onion rings ', 'Vegetarian soup', '3365528c-efeb-4f25-a8c7-032e368e064a', 'en','b579c480-db88-4188-a6e7-175af19ad1cc');
INSERT INTO menus_dishes_cross (menu_id, dish_id) values ('3a66b9e3-925a-4c81-adb6-5988d22dda8a', '3365528c-efeb-4f25-a8c7-032e368e064a');

INSERT INTO dishes (id, ordering_weight, price, status, dish_category_id, user_id) VALUES ('f7d75a24-ae22-4590-bac1-9dfbaa4b3719', 1, 25, 'ACTIVE', 'c4fb846a-0434-439e-a906-7fcb5c9b876b', 'b579c480-db88-4188-a6e7-175af19ad1cc');
INSERT INTO dish_details (id, default_details, description, name, dish_id, locale, user_id) VALUES ('503bd7bc-b2ce-11e9-b8a3-2a2ae2dbcce4', true, 'Ferrero pleasure', 'Ferrero souffle covered in chocolate', 'f7d75a24-ae22-4590-bac1-9dfbaa4b3719', 'en','b579c480-db88-4188-a6e7-175af19ad1cc');
INSERT INTO menus_dishes_cross (menu_id, dish_id) values ('3a66b9e3-925a-4c81-adb6-5988d22dda8a', 'f7d75a24-ae22-4590-bac1-9dfbaa4b3719');

INSERT INTO dishes (id, ordering_weight, price, status, dish_category_id, user_id) VALUES ('ef806545-1f5f-4d90-860f-2b9e2042e36a', 1, 25.00, 'ACTIVE', '8ed23882-853d-483a-98cf-40ca1be99212', 'b579c480-db88-4188-a6e7-175af19ad1cc');
INSERT INTO dish_details (id, default_details, description, name, dish_id, locale, user_id) VALUES ('503bd7bc-b2af-11e9-a2a3-2a2ae2dbcce4', true, 'Oysters in verjus sauce', 'Oysters', 'ef806545-1f5f-4d90-860f-2b9e2042e36a', 'en','b579c480-db88-4188-a6e7-175af19ad1cc');
INSERT INTO menus_dishes_cross (menu_id, dish_id) values ('3a66b9e3-925a-4c81-adb6-5988d22dda8a', 'ef806545-1f5f-4d90-860f-2b9e2042e36a');

INSERT INTO dishes (id, ordering_weight, price, status, dish_category_id, user_id) VALUES ('862f6e31-a760-44ab-ba11-896e9d9479da', 2, 89.00, 'ACTIVE', '8ed23882-853d-483a-98cf-40ca1be99212', '0d949a24-e44f-4cf8-ab58-40894d1ca756');
INSERT INTO dish_details (id, default_details, description, name, dish_id, locale, user_id) VALUES ('503bd8f2-b2ce-11e9-a2a3-2a2ae2dbcce4', true, 'Fish of the day cooked in fish stock', 'Braised fish', '862f6e31-a760-44ab-ba11-896e9d9479da', 'en', '0d949a24-e44f-4cf8-ab58-40894d1ca756');
INSERT INTO menus_dishes_cross (menu_id, dish_id) values ('293f2a14-6672-4111-bc48-ccf66a7eedb1', '862f6e31-a760-44ab-ba11-896e9d9479da');

INSERT INTO dishes (id, ordering_weight, price, status, dish_category_id, user_id) VALUES ('03645408-5b6c-4c02-b3b0-2ff7dfdb1cd2', 4, 85.00, 'ACTIVE', '8ed23882-853d-483a-98cf-40ca1be99212', 'b579c480-db88-4188-a6e7-175af19ad1cc');
INSERT INTO dish_details (id, default_details, description, name, dish_id, locale, user_id) VALUES ('503bda32-b2ce-11e9-a2a3-2a2ae2dbcce4', true, 'Fish of the day marinated with lemon juice', 'Marinated fish', '03645408-5b6c-4c02-b3b0-2ff7dfdb1cd2', 'en', 'b579c480-db88-4188-a6e7-175af19ad1cc');
INSERT INTO menus_dishes_cross (menu_id, dish_id) values ('3a66b9e3-925a-4c81-adb6-5988d22dda8a', '03645408-5b6c-4c02-b3b0-2ff7dfdb1cd2');