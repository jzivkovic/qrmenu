package hr.mars.qrm.repository;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;
import hr.mars.qrm.domain.Menu;
import hr.mars.qrm.domain.Restaurant;
import hr.mars.qrm.domain.User;
import hr.mars.qrm.fixtures.processor.JPAPersistingProcessor;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class RestaurantRepositoryTest {

    @Autowired
    public TestEntityManager entityManager;

    @Autowired
    public JdbcTemplate jdbcTemplate;

    @Autowired
    public RestaurantRepository restaurantRepository;

    JPAPersistingProcessor hibernateProcessor;

    @BeforeClass
    public static void loadTemplates(){
        FixtureFactoryLoader.loadTemplates("hr.mars.qrm.fixtures");
    }

    @Before
    public void initTests() {
        jdbcTemplate.execute("delete FROM dish_details;\n" +
                "delete from menus_dishes_cross;\n" +
                "delete from dishes;\n" +
                "delete from menus;\n" +
                "delete from restaurants;\n" +
                "delete dish_categories;\n"+
                "delete from users_roles_cross;\n"+
                "delete from users;\n");

        hibernateProcessor = new JPAPersistingProcessor(entityManager.getEntityManager());

    }

    @Test
    public void findById_givenUUIDOfRestaurantWithOneMenu_returnsRestaurantEntityWithCorrectMenuInMenuList(){

        List<Restaurant> restaurants = Fixture.from(Restaurant.class).uses(hibernateProcessor).gimme(3,"basic");
        List<Menu> menus = Fixture.from(Menu.class).uses(hibernateProcessor).gimme(3,"basic");
        for (int i = 0; i < restaurants.size() ; i++) {
            menus.get(i).setRestaurant(restaurants.get(i));
            restaurants.get(i).getMenus().add(menus.get(i));
        }

        Restaurant foundRestaurant = restaurantRepository.findById(restaurants.get(0).getId()).get();

        assertNotNull("Restourant entity not found", foundRestaurant);
        assertEquals("Number of menus is not correct", 1, foundRestaurant.getMenus().size());
        assertEquals("Returned menu not as expected", menus.get(0), foundRestaurant.getMenus().iterator().next());
    }

    @Test
    public void findByUserId_givenUserId_ReturnsListOfAssociatedRestaurants(){

        List<User> users = Fixture.from(User.class).uses(hibernateProcessor).gimme(2, "basic");
        List<Restaurant> restaurants = Fixture.from(Restaurant.class).uses(hibernateProcessor).gimme(6, "basic");
        for (int i = 0; i < restaurants.size(); i++) {
            restaurants.get(i).setUser(users.get(i%2));
        }

        List<Restaurant> testUserRestaurants = restaurantRepository.findByUserId(users.get(0).getId());

        assertEquals("wrong number of Restaurants retuned", 3, testUserRestaurants.size());
        assertArrayEquals("wrong restaurants returned",
                restaurants.stream().filter(restaurant -> restaurant.getUser().equals(users.get(0))).toArray(),
                testUserRestaurants.toArray());
    }
}
