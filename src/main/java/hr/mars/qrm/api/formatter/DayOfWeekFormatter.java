package hr.mars.qrm.api.formatter;

import org.springframework.format.Formatter;

import java.text.ParseException;
import java.time.DayOfWeek;
import java.time.format.TextStyle;
import java.util.Locale;

public class DayOfWeekFormatter implements Formatter<DayOfWeek> {

    @Override
    public DayOfWeek parse(String s, Locale locale) throws ParseException {
        return DayOfWeek.of(Integer.parseInt(s));
    }

    @Override
    public String print(DayOfWeek dayOfWeek, Locale locale) {
        locale = Locale.ENGLISH;//for test
        return dayOfWeek.getDisplayName(TextStyle.FULL, locale);
    }
}
