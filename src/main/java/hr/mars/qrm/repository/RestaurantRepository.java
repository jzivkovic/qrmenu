package hr.mars.qrm.repository;

import hr.mars.qrm.domain.Restaurant;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RepositoryRestResource
@Repository
public interface RestaurantRepository extends JpaRepository<Restaurant,UUID>{

    @Override
    @RestResource
    List<Restaurant> findAll();

    @Override
    @RestResource
    List<Restaurant> findAll(Sort sort);

    @Override
    @RestResource
    Page<Restaurant> findAll(Pageable pageable);

    @Override
    @RestResource
    //@PostFilter("filterObject.userId == principal.userId or hasRole('ADMIN')")
    Optional<Restaurant> findById(UUID uuid);

    @RestResource
    @Query("select r from Restaurant r where r.user.id = :userId")
    List<Restaurant> findByUserId(@Param("userId") UUID userId);

    @Override
    @RestResource
    //@PreAuthorize("#R.user.id == principal.userId or hasRole('ADMIN')")
    Restaurant save(@Param("R") Restaurant s);

    @Override
    @RestResource
    //@PreAuthorize("#R.user.id = principal.userId or hasRole('ADMIN')")
    void delete(@Param("R") Restaurant restaurant);

}
