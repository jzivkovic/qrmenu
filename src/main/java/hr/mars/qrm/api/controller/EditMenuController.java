package hr.mars.qrm.api.controller;

import hr.mars.qrm.api.dto.MenuDTO;
import hr.mars.qrm.domain.Menu;
import hr.mars.qrm.service.MenuService;
import hr.mars.qrm.service.RestaurantService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.UUID;

@Controller
public class EditMenuController {

    @Autowired
    MenuService menuService;

    @Autowired
    RestaurantService restaurantService;

    @Autowired
    ModelMapper modelMapper;

    @RequestMapping(value="/menu/{menuId}", method=RequestMethod.GET)
    @ResponseBody
    public MenuDTO get(@PathVariable(name = "menuId") UUID menuId, Model model){

        Menu menu = menuService.get(menuId);

        MenuDTO menuDTO = modelMapper.map(menu, MenuDTO.class);

        model.addAttribute("menu", menuDTO);
        model.addAttribute("user", menu.getUser());

        return menuDTO;
    }

    @RequestMapping(value="/menu/{menuId}", method = RequestMethod.PUT)
    public String save( MenuDTO menuDTO, Model model){

        Menu menu = menuService.get(menuDTO.getId());

        if(menu == null ){
            throw new RuntimeException();
        }

        modelMapper.map(menuDTO, menu);

        menuService.save(menu);

        return "";//get(menuDTO.getId(), model);
    }

    @RequestMapping(value="/menu/{menuId}", method = RequestMethod.DELETE)
    public void delete(@PathVariable(name = "menuId") UUID menuId){

        menuService.delete(menuId);

    }
}
