package hr.mars.qrm.service.impl;

import hr.mars.qrm.domain.Menu;
import hr.mars.qrm.domain.Restaurant;
import hr.mars.qrm.domain.Status;
import hr.mars.qrm.repository.MenuRepository;
import hr.mars.qrm.repository.RestaurantRepository;
import hr.mars.qrm.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@Service
public class MenuServiceImpl implements MenuService {

    @Autowired
    MenuRepository menuRepository;

    @Autowired
    RestaurantRepository restaurantRepository;

    @Override
    public Menu getTodaysMenu(UUID restaurantId) {

        Sort sort = Sort.by(Sort.Direction.DESC, "validOnDate");

        List<Menu> menus = menuRepository.findByRestaurantIdAndStatus(restaurantId, Status.ACTIVE, sort);

        LocalDate today = LocalDate.now();

        for (Menu menu : menus){
            if(menu.getValidOnDate() != null){
                if(menu.getValidOnDate().equals(today)){
                    return menu;
                }else if(menu.getValidOnDate().isBefore(today)){
                    break;
                }
            }
        }

        for (Menu menu : menus){
            if(menu.getValidOnDayOfWeek() != null && menu.getValidOnDayOfWeek().equals(today.getDayOfWeek())){
                return menu;
            }
        }

        for (Menu menu : menus){
            if(menu.getValidAlways()!=null && menu.getValidAlways()){
                return menu;
            }
        }

        return null;
    }

    @Override
    public Menu get(UUID id) {
        return menuRepository.findById(id).get();
    }

    @Override
    public List<Menu> getAll() {
        return menuRepository.findAll();
    }

    @Override
    public Menu create() {

        Menu menu = new Menu();

        return menu;
    }

    @Override
    public Menu create(UUID restaurantId) {

        Restaurant restaurant = restaurantRepository.findById(restaurantId).get();

        if(restaurant == null){
            //TODO ERROR HANDLING
            throw new RuntimeException();
        }

        Menu menu = new Menu();

        menu.setRestaurant(restaurant);
        menu.setUser(restaurant.getUser());
        menu.setStatus(Status.INACTIVE);

        return menu;
    }

    @Override
    public void delete(UUID id) {
        menuRepository.deleteById(id);
    }

    @Override
    public Menu save(Menu menu) {
        return menuRepository.save(menu);
    }
}
