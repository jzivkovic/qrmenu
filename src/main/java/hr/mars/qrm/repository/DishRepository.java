package hr.mars.qrm.repository;

import hr.mars.qrm.domain.Dish;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RepositoryRestResource
@Repository
public interface DishRepository extends JpaRepository<Dish, UUID>{

    @Override
    List<Dish> findAll();

    @Override
    List<Dish> findAll(Sort sort);

    @Override
    Page<Dish> findAll(Pageable pageable);

    @Override
    Optional<Dish> findById(UUID uuid);

    @Query(nativeQuery = true, value = "" +
            "select " +
            "D.ID, " +
            "D.USER_ID, " +
            "D.DISH_CATEGORY_ID, " +
            "D.ORDERING_WEIGHT, " +
            "D.PRICE, " +
            "D.STATUS "+
            "from DISHES D join MENUS_DISHES_CROSS MDC " +
            "   on D.ID = MDC.DISH_ID " +
            "left join DISH_CATEGORIES DC " +
            "   ON DC.ID = D.DISH_CATEGORY_ID " +
            "where MDC.MENU_ID = :menuId " +
            "and D.STATUS = :status " +
            "order by DC.ORDERING_WEIGHT desc, D.ORDERING_WEIGHT desc ")
    List<Dish> findByMenuIdAndStatusOrderByCategory_OrderingWeightAndDish_OrderingWeight(@Param("menuId")UUID menuId, @Param(value = "status") String status);

    @Override
    //@PreAuthorize("#D.userId == principal.userId or hasRole('ADMIN')")
    Dish save(@Param("D") Dish s);

    @Override
    //@PreAuthorize("#D.userId = principal.userId or hasRole('ADMIN')")
    void delete(@Param("D") Dish Dish);
}
