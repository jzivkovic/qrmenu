package hr.mars.qrm.api.dto;

import hr.mars.qrm.domain.Menu;
import hr.mars.qrm.domain.Restaurant;

public class PresentMenuDTO {

    private Restaurant restaurant;
    private Menu menu;

    public PresentMenuDTO(Restaurant restaurant, Menu menu) {
        this.restaurant = restaurant;
        this.menu = menu;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    public Menu getMenu() {
        return menu;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }
}
