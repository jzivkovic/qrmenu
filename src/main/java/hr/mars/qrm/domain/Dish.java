package hr.mars.qrm.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

@Entity
@Table(name="dishes")
public class Dish {

    public Dish() {
    }

    @Id
    @GeneratedValue
    private UUID id;

    @ManyToOne
    @NotNull
    private User user;

    @ManyToOne
    @JoinColumn(name="DISH_CATEGORY_ID")
    private DishCategory dishCategory;

    private int orderingWeight;

    @NotNull
    private BigDecimal price;

    @Enumerated(EnumType.STRING)
    @NotNull
    private Status status;

    @OneToMany(mappedBy="dish",cascade = CascadeType.ALL)
    @MapKey(name="locale")
    private Map<Locale, DishDetails> dishDetails;

    private Dish(Builder builder) {
        setId(builder.id);
        user = builder.user;
        setDishCategory(builder.dishCategory);
        setOrderingWeight(builder.orderingWeight);
        setPrice(builder.price);
        setStatus(builder.status);
        setDishDetails(builder.dishDetails);
    }

    public static Builder newBuilder() {
        return new Builder();
    }


    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public DishCategory getDishCategory() {
        return dishCategory;
    }

    public void setDishCategory(DishCategory dishCategory) {
        this.dishCategory = dishCategory;
    }

    public int getOrderingWeight() {
        return orderingWeight;
    }

    public void setOrderingWeight(int orderingWeight) {
        this.orderingWeight = orderingWeight;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Map<Locale, DishDetails> getDishDetails() {
        return dishDetails;
    }

    public void setDishDetails(Map<Locale, DishDetails> dishDetails) {
        this.dishDetails = dishDetails;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public static final class Builder {
        private UUID id;
        private @NotNull User user;
        private DishCategory dishCategory;
        private int orderingWeight;
        private @NotNull BigDecimal price;
        private @NotNull Status status;
        private @NotNull Map<Locale, DishDetails> dishDetails;

        private Builder() {
        }

        public Builder withId(UUID val) {
            id = val;
            return this;
        }

        public Builder withUser(@NotNull User val) {
            user = val;
            return this;
        }

        public Builder withCategory(DishCategory val) {
            dishCategory = val;
            return this;
        }

        public Builder withOrderingWeight(int val) {
            orderingWeight = val;
            return this;
        }

        public Builder withPrice(@NotNull BigDecimal val) {
            price = val;
            return this;
        }

        public Builder withStatus(@NotNull Status val) {
            status = val;
            return this;
        }

        public Builder withDishDetails(@NotNull Map<Locale, DishDetails> val) {
            dishDetails = val;
            return this;
        }

        public Dish build() {
            return new Dish(this);
        }
    }
}
