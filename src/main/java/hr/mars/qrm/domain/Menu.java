package hr.mars.qrm.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name="menus")
public class Menu {

    public Menu() {
    }

    @Id
    @GeneratedValue
    private UUID id;

    @ManyToOne
    @NotNull
    private User user;

    private String name;

    private LocalDate validOnDate;

    private DayOfWeek validOnDayOfWeek;

    private Boolean validAlways;

    @Enumerated(EnumType.STRING)
    @NotNull
    private Status status;

    @ManyToOne
    @JoinColumn(name="restaurant_id")
    @NotNull
    private Restaurant restaurant;

    @ManyToMany(
            cascade = CascadeType.ALL,
            fetch = FetchType.LAZY)
    @JoinTable(
            name="menus_dishes_cross",
            joinColumns=@JoinColumn(name="menu_id", referencedColumnName="id"),
            inverseJoinColumns=@JoinColumn(name="dish_id", referencedColumnName="id"))
    private List<Dish> dishes;

    private Menu(Builder builder) {
        setId(builder.id);
        setUser(builder.user);
        setName(builder.name);
        setValidOnDate(builder.validOnDate);
        setValidOnDayOfWeek(builder.validOnDayOfWeek);
        setValidAlways(builder.validEndless);
        setStatus(builder.status);
        setRestaurant(builder.restaurant);
        setDishes(builder.dishes);
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getValidOnDate() {
        return validOnDate;
    }

    public void setValidOnDate(LocalDate validOnDate) {
        this.validOnDate = validOnDate;
    }

    public DayOfWeek getValidOnDayOfWeek() {
        return validOnDayOfWeek;
    }

    public void setValidOnDayOfWeek(DayOfWeek validOnDayOfWeek) {
        this.validOnDayOfWeek = validOnDayOfWeek;
    }

    public Boolean getValidAlways() {
        return validAlways;
    }

    public void setValidAlways(Boolean validAlways) {
        this.validAlways = validAlways;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    public List<Dish> getDishes() {
        return dishes;
    }

    public void setDishes(List<Dish> dishes) {
        this.dishes = dishes;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }


    public static final class Builder {
        private UUID id;
        private @NotNull User user;
        private String name;
        private LocalDate validOnDate;
        private DayOfWeek validOnDayOfWeek;
        private Boolean validEndless;
        private @NotNull Status status;
        private @NotNull Restaurant restaurant;
        private List<Dish> dishes;

        public Builder() {
        }

        public Builder withId(UUID val) {
            id = val;
            return this;
        }

        public Builder withUser(@NotNull User val) {
            user = val;
            return this;
        }

        public Builder withName(String val) {
            name = val;
            return this;
        }

        public Builder withValidOnDate(LocalDate val) {
            validOnDate = val;
            return this;
        }

        public Builder withValidOnDayOfWeek(DayOfWeek val) {
            validOnDayOfWeek = val;
            return this;
        }

        public Builder withValidEndless(Boolean val) {
            validEndless = val;
            return this;
        }

        public Builder withStatus(@NotNull Status val) {
            status = val;
            return this;
        }

        public Builder withRestaurant(@NotNull Restaurant val) {
            restaurant = val;
            return this;
        }

        public Builder withDishes(List<Dish> val) {
            dishes = val;
            return this;
        }

        public Menu build() {
            return new Menu(this);
        }
    }
}
