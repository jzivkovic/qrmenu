package hr.mars.qrm.api.dto;

import hr.mars.qrm.domain.Status;
import hr.mars.qrm.domain.Town;

import java.util.Set;
import java.util.UUID;

public class RestaurantDTO {

    private UUID id;

    private String name;

    private String address;

    private Town town;

    private String locationXCoordinate;

    private String locationYCoordinate;

    private Set<MenuDTO> menus;

    private String phoneNumber;

    private String openingHours;

    private Status status;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Town getTown() {
        return town;
    }

    public void setTown(Town town) {
        this.town = town;
    }

    public String getLocationXCoordinate() {
        return locationXCoordinate;
    }

    public void setLocationXCoordinate(String locationXCoordinate) {
        this.locationXCoordinate = locationXCoordinate;
    }

    public String getLocationYCoordinate() {
        return locationYCoordinate;
    }

    public void setLocationYCoordinate(String locationYCoordinate) {
        this.locationYCoordinate = locationYCoordinate;
    }

    public Set<MenuDTO> getMenus() {
        return menus;
    }

    public void setMenus(Set<MenuDTO> menus) {
        this.menus = menus;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getOpeningHours() {
        return openingHours;
    }

    public void setOpeningHours(String openingHours) {
        this.openingHours = openingHours;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
