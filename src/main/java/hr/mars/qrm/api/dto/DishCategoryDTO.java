package hr.mars.qrm.api.dto;

import javax.validation.constraints.NotNull;
import java.util.UUID;

public class DishCategoryDTO {

    private UUID id;

    @NotNull
    private String name;

    private int orderingWeight;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getOrderingWeight() {
        return orderingWeight;
    }

    public void setOrderingWeight(int orderingWeight) {
        this.orderingWeight = orderingWeight;
    }
}
