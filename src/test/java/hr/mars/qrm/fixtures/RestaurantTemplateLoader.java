package hr.mars.qrm.fixtures;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;
import hr.mars.qrm.domain.Menu;
import hr.mars.qrm.domain.Restaurant;
import hr.mars.qrm.domain.Status;
import hr.mars.qrm.domain.User;

import java.util.Random;

import static hr.mars.qrm.fixtures.function.Functions.arrayList;


public class RestaurantTemplateLoader implements TemplateLoader{

    @Override
    public void load() {
        Fixture.of(Restaurant.class).addTemplate("basic", new Rule(){{
            //add("id", UUID.randomUUID());
            add("name", "Restaurant " + name());
            add("address", "Ulica " + new Random().nextInt());
            add("location", new Restaurant.Location( 1, 28));
            add("status", Status.ACTIVE);
            add("menus", arrayList(Menu.class));
            add("user", one(User.class,"basic"));
        }});
    }
}
