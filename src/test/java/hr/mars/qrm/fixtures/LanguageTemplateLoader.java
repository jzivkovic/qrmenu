package hr.mars.qrm.fixtures;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;

import java.util.Locale;
import java.util.UUID;

public class LanguageTemplateLoader implements TemplateLoader {

    static UUID englishId = UUID.randomUUID();

    @Override
    public void load() {
        Fixture.of(Locale.class).addTemplate("enlglish", new Rule(){{
            //add("id", englishId);
            add("name", "English");
            add("code", "EN");
        }});
    }
}
