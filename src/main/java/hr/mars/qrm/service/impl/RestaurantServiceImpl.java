package hr.mars.qrm.service.impl;

import hr.mars.qrm.domain.Restaurant;
import hr.mars.qrm.domain.Status;
import hr.mars.qrm.domain.User;
import hr.mars.qrm.repository.RestaurantRepository;
import hr.mars.qrm.repository.UserRepository;
import hr.mars.qrm.service.RestaurantService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.UUID;

public class RestaurantServiceImpl implements RestaurantService {

    @Autowired
    RestaurantRepository restaurantRepository;

    @Autowired
    UserRepository userRepository;

    @Override
    public Restaurant get(UUID id) {
        return restaurantRepository.findById(id).get();
    }

    @Override
    public List<Restaurant> getAll() {
        return restaurantRepository.findAll();
    }

    @Override
    public List<Restaurant> getAllForUser(UUID userId) {
        return restaurantRepository.findByUserId(userId);
    }

    @Override
    public Restaurant create() {

        Restaurant restaurant = new Restaurant();

        return restaurant;
    }

    @Override
    public Restaurant create(UUID userId) {

        User user = userRepository.findById(userId).get();

        if(user == null){
            //TODO ERROR HANDLING
            throw new RuntimeException();
        }

        Restaurant restaurant = new Restaurant();

        restaurant.setUser(user);
        restaurant.setStatus(Status.INACTIVE);

        return restaurant;
    }

    @Override
    public void delete(UUID id) {
        restaurantRepository.deleteById(id);
    }

    @Override
    public Restaurant save(Restaurant restaurant) {
        return restaurantRepository.save(restaurant);
    }
}
