package hr.mars.qrm.api.dto.converter;

import org.springframework.core.convert.converter.Converter;

import java.time.DayOfWeek;

public class DayOfWeekConverter implements Converter<String, DayOfWeek> {
    @Override
    public DayOfWeek convert(String s) {

        if(s==null || s.isEmpty()){
            return null;
        }

        return DayOfWeek.valueOf(s);
    }
}
