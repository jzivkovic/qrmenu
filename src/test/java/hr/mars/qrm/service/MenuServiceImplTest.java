package hr.mars.qrm.service;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;
import hr.mars.qrm.domain.Menu;
import hr.mars.qrm.domain.Restaurant;
import hr.mars.qrm.repository.MenuRepository;
import hr.mars.qrm.service.impl.MenuServiceImpl;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDate;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MenuServiceImplTest {

    @Mock
    MenuRepository menuRepository;

    @InjectMocks
    MenuServiceImpl menuService;

    @BeforeClass
    public static void loadTemplates(){
        FixtureFactoryLoader.loadTemplates("hr.mars.qrm.fixtures");
    }

    @Test
    public void getTodaysMenu_activeRestaurantId_ValidOnDateMenuReturned(){

        Restaurant restaurant = Fixture.from(Restaurant.class).gimme("basic");

        List<Menu> menus =Fixture.from(Menu.class).gimme(3,
                "valid_on_date_today",
                "valid_on_day_friday",
                "valid_endless");

        for(Menu menu : menus){
            menu.setRestaurant(restaurant);
        }
        restaurant.getMenus().addAll(menus);

        when(menuRepository.findByRestaurantIdAndStatus(any(), any(), any()))
                .thenReturn(menus);

        Menu returnedMenu = menuService.getTodaysMenu(restaurant.getId());

        assertEquals("Wrong todays menu", menus.get(0), returnedMenu);
    }

    @Test
    public void getTodaysMenu_activeRestaurantId_ActiveOnDayMenuReturned(){

        Restaurant restaurant = Fixture.from(Restaurant.class).gimme("basic");

        List<Menu> menus =Fixture.from(Menu.class).gimme(2,
                "valid_endless",
                "valid_endless");

        for(Menu menu : menus){
            menu.setRestaurant(restaurant);
        }
        restaurant.getMenus().addAll(menus);

        menus.get(0).setValidOnDayOfWeek(LocalDate.now().getDayOfWeek());

        when(menuRepository.findByRestaurantIdAndStatus(any(), any(), any()))
                .thenReturn(menus);

        Menu returnedMenu = menuService.getTodaysMenu(restaurant.getId());

        assertEquals("Wrong todays menu", menus.get(0), returnedMenu);
    }

    @Test
    public void getTodaysMenu_activeRestaurantIdEndlesslyActiveMenu_ActiveMenuReturned(){

        Restaurant restaurant = Fixture.from(Restaurant.class).gimme("basic");

        List<Menu> menus =Fixture.from(Menu.class).gimme(3,
                "valid_endless"
                ,"valid_endless"
                ,"valid_endless"
        );

        for(Menu menu : menus){
            menu.setRestaurant(restaurant);
        }
        restaurant.getMenus().addAll(menus);

        menus.get(0).setValidAlways(null);
        menus.get(1).setValidAlways(false);

        when(menuRepository.findByRestaurantIdAndStatus(any(), any(), any()))
                .thenReturn(menus);

        Menu returnedMenu = menuService.getTodaysMenu(restaurant.getId());

        assertEquals("Wrong todays menu", menus.get(2), returnedMenu);
    }

    @Test
    public void getTodaysMenu_activeRestaurantIdNoActiveMenu_NullReturned(){

        Restaurant restaurant = Fixture.from(Restaurant.class).gimme("basic");

        List<Menu> menus =Fixture.from(Menu.class).gimme(2,
                "valid_endless",
                "valid_endless");

        for(Menu menu : menus){
            menu.setRestaurant(restaurant);
        }
        restaurant.getMenus().addAll(menus);

        menus.get(0).setValidAlways(null);
        menus.get(1).setValidAlways(null);

        when(menuRepository.findByRestaurantIdAndStatus(any(), any(), any()))
                .thenReturn(menus);

        Menu returnedMenu = menuService.getTodaysMenu(restaurant.getId());

        assertNull("Should have retunred null", returnedMenu);
    }
}
