package hr.mars.qrm.fixtures;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;
import hr.mars.qrm.domain.User;

public class UserFixTemplateLoader implements TemplateLoader {

    @Override
    public void load() {
        Fixture.of(User.class).addTemplate("basic", new Rule(){{
            //add("id", UUID.randomUUID());
            add("username", name());
            add("password", uniqueRandom("$2a$10$pGSN7hrOIdfKEHMH1W5LfuxM04tBhOOXbvoqzhrvo/ZK/NrzGd3pW"));
            add("enabled", true);
        }});
    }
}
