package hr.mars.qrm.service.impl;

import hr.mars.qrm.domain.Town;
import hr.mars.qrm.repository.TownRepository;
import hr.mars.qrm.service.TownService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class TownServiceImpl implements TownService {

    @Autowired
    TownRepository townRepository;

    @Override
    public List<Town> findAll() {
        return townRepository.findAll();
    }
}
