package hr.mars.qrm.repository;

import hr.mars.qrm.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@RepositoryRestResource(exported = false)
@Repository
public interface UserRepository extends JpaRepository<User, UUID> {

    User findByUsername(String username);
}
