package hr.mars.qrm.api.dto;

import java.util.UUID;

public class OpeningHoursDTO {

    private UUID id;

    private String description;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
