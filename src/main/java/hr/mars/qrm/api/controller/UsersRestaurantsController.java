package hr.mars.qrm.api.controller;

import hr.mars.qrm.api.dto.RestaurantDTO;
import hr.mars.qrm.domain.Restaurant;
import hr.mars.qrm.service.RestaurantService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Controller
public class UsersRestaurantsController {

    @Autowired
    RestaurantService restaurantService;

    @Autowired
    ModelMapper modelMapper;

    @Autowired
    UIUtil UIUtilInstance;

    //@Cacheable(CURRENTLY_ACTIVE_MENU)
    @GetMapping("/usersRestaurants/{userId}")
    public String getPresentMenuDto(@PathVariable(name = "userId") UUID userId, Model model){

        List<Restaurant> restaurants = restaurantService.getAllForUser(userId);

        List<RestaurantDTO> restaurantDTOs =  restaurants.stream().map(restaurant -> modelMapper.map(restaurant, RestaurantDTO.class)).collect(Collectors.toList());

        model.addAttribute("restaurants", restaurantDTOs);
        model.addAttribute("UIUtil", UIUtilInstance);

        return "usersRestaurants";
    }

}
